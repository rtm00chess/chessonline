﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ChessCore.Data;

namespace ChessData.Domain.Roles
{
	public class Role : BaseEntity 
	{
		public string Name { get; set; }

		public int RightLevelId { get; set; }

        /// <summary>
        /// Gets or sets the RightLevel
        /// </summary>
        public RightLevel RightLevel
        {
            get
            {
                return (RightLevel)this.RightLevelId;
            }
            set
            {
                this.RightLevelId = (int)value;
            }
        }		
	}
}
