﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ChessData.Domain
{
    public enum RightLevel
    {
        /// <summary>
        /// Adminstrator
        /// </summary>
        Adminstrator = 10,
        /// <summary>
        /// User
        /// </summary>
        User = 20,
        /// <summary>
        /// Guest
        /// </summary>
        Guest = 30,
    }
}
