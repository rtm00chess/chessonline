﻿using System;
using System.Collections.Generic;
using System.Linq;
using ChessData.Domain.Roles;
using System.Text;
using System.Threading.Tasks;
using ChessCore.Data;

namespace ChessData.Domain.Customer
{
    public class Customer : BaseEntity
    {
        public string Name { get; set; }

		public IList<Role> Role{ get; set; }

		public Guid Token{ get; set; }
	}
}	
